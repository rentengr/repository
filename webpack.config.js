const { join } = require('path');

const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
  mode: 'production',
  entry: './src/main.ts',
  target: 'node',
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.json']
  },
  node: {
    __dirname: false
  },
  output: {
    filename: 'bundle.js',
    path: join(__dirname, 'dist'),
    libraryTarget: 'commonjs'
  },
  plugins: [
    new CleanWebpackPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      }
    ]
  }
};
