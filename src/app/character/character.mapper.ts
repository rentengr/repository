import * as moment from 'moment';

import { ICharacterDto, ICharacterModel } from './character.model';

class CharacterMapper {
  dtoToModel(dto: ICharacterDto): ICharacterModel {
    return {
      _id: dto.id,
      firstName: dto.firstName,
      lastName: dto.lastName,
      birthDate: moment(dto.birthDate).toDate()
    };
  }

  modelToDto(model: ICharacterModel): ICharacterDto {
    return {
      id: model._id,
      firstName: model.firstName,
      lastName: model.lastName,
      birthDate: moment(model.birthDate.valueOf()).format()
    };
  }
}

export const characterMapper = new CharacterMapper();
