import { NextFunction, Request, Response, Router } from 'express';

import { PageRequest, PageResponse } from '../common/pagination/pagination';
import { pageMiddlewares } from '../common/pagination/pagination.handler';

import { characterController } from './character.controller';
import { ICharacterDto } from './character.model';

export const characterRouter = Router()
  .get('/', ...pageMiddlewares,
    (req: PageRequest, res: PageResponse<ICharacterDto>, next: NextFunction) => characterController.find(req, res, next))
  .get('/:id', (req: Request, res: Response, next: NextFunction) => characterController.get(req, res, next))
  .post('/', (req: Request, res: Response, next: NextFunction) => characterController.create(req, res, next))
  .put('/:id', (req: Request, res: Response, next: NextFunction) => characterController.update(req, res, next))
  .delete('/:id', (req: Request, res: Response, next: NextFunction) => characterController.remove(req, res, next));
