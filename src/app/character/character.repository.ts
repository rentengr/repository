import { AppError, AppErrorType } from '../common/error/error';
import { invalidId, notFound } from '../common/error/mongoose-error.util';
import { PageParams } from '../common/pagination/pagination';

import { CharacterModel, ICharacterModel } from './character.model';

class CharacterRepository {

  find({ sort, skip, limit }: PageParams): Promise<ICharacterModel[]> {
    return CharacterModel.find()
      .sort(sort)
      .skip(skip)
      .limit(limit)
      .then(entities => entities);
  }

  count(): Promise<number> {
    return CharacterModel.countDocuments()
      .then(count => count);
  }

  get(id: string): Promise<ICharacterModel> {
    return CharacterModel.findById(id)
      .then(entity => {
        if (entity) {
          return entity;
        }
        const error = new AppError(AppErrorType.RESOURCE_NOT_FOUND, `No match with id=${id} has been found`);
        return Promise.reject(error);
      })
      .catch(invalidId(id));
  }

  create(entity: ICharacterModel): Promise<ICharacterModel> {
    const entityToSave = new CharacterModel(entity);
    return entityToSave.save();
  }

  update(entity: ICharacterModel): Promise<ICharacterModel> {
    const entityToSave = new CharacterModel(entity);
    entityToSave.isNew = false;
    return entityToSave.save()
      .catch(notFound(entity._id))
      .catch(invalidId(entity._id));
  }

  remove(id: string): Promise<void> {
    return CharacterModel.deleteOne({ _id: id })
      .then(response => {
        if (!response.n) {
          return Promise.reject(new AppError(AppErrorType.RESOURCE_NOT_FOUND, `No match with id=${id} has been found`));
        }
      })
      .catch(invalidId(id));
  }

}

export const characterRepository = new CharacterRepository();
