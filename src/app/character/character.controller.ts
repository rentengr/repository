import * as Joi from '@hapi/joi';
import { NextFunction, Request, Response } from 'express';

import { joiDtoError, joiPathParameterError } from '../common/joi-validation.util';
import { PageRequest, PageResponse } from '../common/pagination/pagination';
import { QueryParams } from '../common/query-params.util';

import { CharacterDtoSchema, ICharacterDto } from './character.model';
import { characterService } from './character.service';

class CharacterController {

  find(req: PageRequest, res: PageResponse<ICharacterDto>, next: NextFunction): void {
    Promise.resolve()
      .then(() => Promise.all([
        Joi.string()
          .valid(...['firstName', 'lastName', 'birthDate', 'id'].reduce((acc, field) => [...acc, field, `-${field}`], []))
          .optional()
          .validateAsync(req.query.sort),
        Joi.number().max(100).validateAsync(parseInt((req.query as QueryParams).size, 10) || 10),
        Joi.number().max(100).validateAsync(parseInt((req.query as QueryParams).page, 10) || 0)
      ]))
      .catch(joiPathParameterError)
      .then(() => characterService.find(req.pageParams))
      .then(page => res.jsonPage(page))
      .catch(next);
  }

  get(req: Request, res: Response, next: NextFunction): void {
    Promise.resolve(req.params)
      .then(({ id }) => Joi.string().required().validateAsync(id))
      .catch(joiPathParameterError)
      .then((id) => characterService.get(id))
      .then(dto => res.json(dto))
      .catch(next);
  }

  create(req: Request, res: Response, next: NextFunction): void {
    Promise.resolve(req.body)
      .then(dto => Joi.string().max(0).validateAsync(dto.id)
        .then(() => CharacterDtoSchema.validateAsync(dto)))
      .catch(joiDtoError)
      .then(dto => characterService.create(dto))
      .then(dto => res.status(201).json(dto))
      .catch(next);
  }

  update(req: Request, res: Response, next: NextFunction): void {
    Promise.resolve([req.params, req.body])
      .then(([{ id }, dto]) => Promise.all([
        Joi.string().required().validateAsync(id).catch(joiPathParameterError),
        CharacterDtoSchema.validateAsync(dto).catch(joiDtoError)
      ]))
      .then(([id, dto]) => characterService.update({ ...dto, id }))
      .then(dto => res.json(dto))
      .catch(next);
  }

  remove(req: Request, res: Response, next: NextFunction): void {
    Promise.resolve(req.params)
      .then(({ id }) => Joi.string().required().validateAsync(id))
      .catch(joiPathParameterError)
      .then((id) => characterService.remove(id))
      .then(() => res.sendStatus(204))
      .catch(next);
  }

}

export const characterController = new CharacterController();
