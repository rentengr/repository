import { Page, PageParams } from '../common/pagination/pagination';
import { mapPage } from '../common/pagination/pagination.handler';

import { characterMapper } from './character.mapper';
import { ICharacterDto, ICharacterModel } from './character.model';
import { characterRepository } from './character.repository';

class CharacterService {

  find(pageParams: PageParams): Promise<Page<ICharacterDto>> {
    return Promise.resolve()
      .then(() => Promise.all([
        characterRepository.find(pageParams),
        characterRepository.count()
      ]))
      .then(([items, count]) => ({ items, count }))
      .then((page: Page<ICharacterModel>) => mapPage(page, model => characterMapper.modelToDto(model)));
  }

  get(id: string): Promise<ICharacterDto> {
    return Promise.resolve()
      .then(() => characterRepository.get(id))
      .then(model => characterMapper.modelToDto(model));
  }

  create(dto: ICharacterDto): Promise<ICharacterDto> {
    return Promise.resolve()
      .then(() => characterMapper.dtoToModel(dto))
      .then(model => characterRepository.create(model))
      .then(model => characterMapper.modelToDto(model));
  }

  update(dto: ICharacterDto): Promise<ICharacterDto> {
    return Promise.resolve()
      .then(() => characterMapper.dtoToModel(dto))
      .then(model => characterRepository.update(model))
      .then(model => characterMapper.modelToDto(model));
  }

  remove(id: string): Promise<void> {
    return Promise.resolve()
      .then(() => characterRepository.remove(id));
  }

}

export const characterService = new CharacterService();
