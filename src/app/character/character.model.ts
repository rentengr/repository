import * as Joi from '@hapi/joi';
import { Document, model, Schema } from 'mongoose';

export interface ICharacterDto {
  id: string;
  firstName: string;
  lastName: string;
  birthDate: string;
}

export interface ICharacterModel {
  _id: string;
  firstName: string;
  lastName: string;
  birthDate: Date;
}

const modelSchema = new Schema({
  firstName: String,
  lastName: String,
  birthDate: Date
});
export const CharacterModel = model<ICharacterModel & Document>('Character', modelSchema);

export const CharacterDtoSchema = Joi.object({
  id: Joi.string(),
  firstName: Joi.string().required(),
  lastName: Joi.string().required(),
  birthDate: Joi.string().isoDate().required()
});
