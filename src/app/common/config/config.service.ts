import * as _ from 'lodash';

import { environment as base } from '../../../environment/environment';
import { environment as dev } from '../../../environment/environment.dev';
import { environment as staging } from '../../../environment/environment.staging';
import { environment as test } from '../../../environment/environment.test';

import { Config } from './config';

const configs = { base, dev, staging, test };
const arrayMerge = (destination, source) => {
  if (_.isArray(destination) && _.isArray(source)) {
    return source;
  }
};
const config = _.mergeWith(
  {}, configs.base, configs[process.env.ENVIRONMENT] || {},
  arrayMerge);

class ConfigService {

  get config(): Config {
    return config;
  }

}

export const configService = new ConfigService();
