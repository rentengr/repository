import { expect } from 'chai';

import { configService } from './config.service';

describe('ConfigService', () => {

  it('should pick correct environment config', () => {
    const config = configService.config.current;

    expect(config).to.equal('test');
  });

});
