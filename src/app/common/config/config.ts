export interface Config {
  current?: 'base' | 'dev' | 'staging' | 'test';
  version?: string;
  logger?: {
    level?: string;
  };
  mongo?: {
    name?: string;
    domain?: string;
    username?: string;
    password?: string;
  };
  business?: {};
}
