export class AppErrorType {
  static readonly BAD_FORMAT_JSON = new AppErrorType('BAD_FORMAT_JSON', 400, 'The provided body does not meet expected JSON format');
  static readonly PATH_PARAMETER_INVALID_FORMAT
    = new AppErrorType('PATH_PARAMETER_INVALID_FORMAT', 400, 'Path parameter format is invalid');
  static readonly DTO_INVALID_FORMAT = new AppErrorType('DTO_INVALID_FORMAT', 400, 'The DTO format is invalid');
  static readonly DTO_INVALID_ID = new AppErrorType('DTO_INVALID_ID', 400, 'The DTO id is invalid');
  static readonly RESOURCE_NOT_FOUND = new AppErrorType('NOT_FOUND', 404, 'The requested resource has not been found');
  static readonly ENDPOINT_NOT_FOUND = new AppErrorType('ENDPOINT_NOT_FOUND', 404, 'The endpoint has not been found');
  static readonly NOT_AUTHENTICATED = new AppErrorType('NOT_AUTHENTICATED', 401, 'The requested resource requires authentication');
  static readonly AUTHENTICATION_ERROR = new AppErrorType('AUTHENTICATION_ERROR', 401, 'No user found with given credentials');
  static readonly NOT_ALLOWED = new AppErrorType('NOT_ALLOWED', 403, 'The requested resource requires specific granted rights');
  static readonly INTERNAL_ERROR = new AppErrorType('INTERNAL_ERROR', 500, 'An error has occurred while processing the request');
  static readonly REMOTE_API_ERROR = new AppErrorType('REMOTE_API_ERROR', 502, 'Remote API has sent a blocking error');

  constructor(public code: string,
              public httpCode: number,
              public description: string) {
  }
}

export class AppError extends Error {

  constructor(public type: AppErrorType,
              message?: string,
              public privateDetails?: any,
              public publicDetails?: any) {
    super(message || type && type.description);
  }

}
