import { AppError, AppErrorType } from './error';

export const invalidId = id => error => {
  if (error.constructor.name === 'CastError') {
    return Promise.reject(new AppError(AppErrorType.DTO_INVALID_ID, `DTO id=${id} format is invalid`));
  }
  return Promise.reject(error);
};

export const notFound = id => error => {
  if (error.name === 'ValidationError') {
    return Promise.reject(new AppError(AppErrorType.RESOURCE_NOT_FOUND, `No match with id=${id} has been found`));
  }
  return Promise.reject(error);
};
