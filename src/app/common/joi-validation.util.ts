import { AppError, AppErrorType } from './error/error';

export const joiDtoError = error => Promise
  .reject(new AppError(AppErrorType.DTO_INVALID_FORMAT, undefined, undefined, error?.details?.[0]?.message));

export const joiPathParameterError = error => Promise
  .reject(new AppError(AppErrorType.PATH_PARAMETER_INVALID_FORMAT, undefined, undefined, error?.details?.[0]?.message));
