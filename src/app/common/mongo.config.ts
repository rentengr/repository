import * as mongoose from 'mongoose';

import { configService } from './config/config.service';

const { name, domain, username, password } = configService.config.mongo;
const url = `mongodb://${username}:${password}@${domain}/${name}?retryWrites=true`;
mongoose.connect(url, {useNewUrlParser: true})
  .catch(console.error);
