import { assert } from 'chai';

import { logger } from './logger';

describe('Logger', () => {

  it('should log without any error', () => {
    try {
      logger.info('log me please');
    } catch (error) {
      assert.fail(error, null, 'logger should not throw error');
    }
    assert.ok(true, 'logger does not throw errors');
  });

});
