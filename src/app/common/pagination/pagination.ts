import { Request, Response } from 'express';

export interface PageParams {
  limit?: number;
  skip?: number;
  sort?: string;
}

export interface Page<T> {
  items: T[];
  count: number;
}

export interface PageRequest extends Request {
  pageParams: PageParams;
}

export type JsonPage<T> = (page: Page<T>) => void;

export interface PageResponse<T> extends Response {
  jsonPage: JsonPage<T>;
}
