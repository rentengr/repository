import { NextFunction, Request, Response } from 'express';

import { QueryParams } from '../query-params.util';

import { Page, PageRequest, PageResponse } from './pagination';

const DEFAULT_SIZE = 10;
const MAX_SIZE = 100;
export const PAGINATION_TOTAL_COUNT_HEADER = 'X-Total-Count';

export const pageParamsMiddleware = (req: Request, res: Response, next: NextFunction) => {
  const { page: pageStr, size: sizeStr, sort } = req.query as QueryParams;
  const page = parseInt(pageStr, 10) || 0;
  const limit = Math.min(parseInt(sizeStr, 10), MAX_SIZE) || DEFAULT_SIZE;
  const skip = page * limit;
  (req as PageRequest).pageParams = { limit, skip, sort };
  next();
};

export const pageResponseMiddleware = <T>(req: Request, res: Response, next: NextFunction) => {
  (res as PageResponse<T>).jsonPage = (page: Page<T>) => {
    res.header(PAGINATION_TOTAL_COUNT_HEADER, page.count.toString());
    res.json(page.items);
  };
  next();
};

export const mapPage = <T, U>(page: Page<T>, itemMapper: (item: T) => U): Page<U> => ({
  ...page,
  items: page.items.map(item => itemMapper(item))
});

export const pageMiddlewares = [pageParamsMiddleware, pageResponseMiddleware];
