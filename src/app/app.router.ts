import { Router } from 'express';

import { characterRouter } from './character/character.router';

export const appRouter = Router()
  .use('/characters', characterRouter);
