import * as compression from 'compression';
import * as cors from 'cors';
import * as express from 'express';
import * as helmet from 'helmet';

import { appRouter } from './app.router';
import { AppError, AppErrorType } from './common/error/error';
import { errorMiddleware } from './common/error/error.handler';
import { expressLogger } from './common/logger/logger';
import './common/mongo.config';
import { PAGINATION_TOTAL_COUNT_HEADER } from './common/pagination/pagination.handler';

export const app = express();

app.use(helmet());
app.use(cors({ exposedHeaders: [PAGINATION_TOTAL_COUNT_HEADER] }));
app.use(express.json(), (error, req, res, next) => next(new AppError(AppErrorType.BAD_FORMAT_JSON)));
app.use(expressLogger);
app.use(compression());
app.use('/api', appRouter);
app.use('*', (req, res, next) => next(new AppError(AppErrorType.ENDPOINT_NOT_FOUND)));
app.use(errorMiddleware);
