import { app } from './app/app';
import { configService } from './app/common/config/config.service';
import { logger } from './app/common/logger/logger';

const port = process.env.PORT || 3000;
app.listen(port, () => logger.info(`Repository app (${configService.config.current}) listening on port ${port}!`));
