import { version } from '../../package.json';
import { Config } from '../app/common/config/config';

export const environment: Config = {
  current: 'base',
  version,
  logger: {
    level: 'info'
  },
  mongo: {
    name: 'alex-del-var_repository_dev',
    domain: 'mongodb-alex-del-var.alwaysdata.net',
    username: 'alex-del-var',
    password: 'Alex=del=var=83'
  }
};
