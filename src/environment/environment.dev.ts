import { Config } from '../app/common/config/config';

export const environment: Config = {
  current: 'dev',
  logger: {
    level: 'debug'
  },
  mongo: {
    name: 'alex-del-var_repository_prod',
    domain: 'mongodb-alex-del-var.alwaysdata.net',
    username: 'alex-del-var',
    password: 'Alex=del=var=83',
  }
};
