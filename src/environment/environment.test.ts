import { Config } from '../app/common/config/config';

export const environment: Config = {
  current: 'test',
  logger: {
    level: 'debug'
  }
};
